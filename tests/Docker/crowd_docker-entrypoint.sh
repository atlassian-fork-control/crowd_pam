#!/bin/bash
if [ "$1" = 'crowd' ]; then
  export CROWD_LICENSE=$(echo $CROWD_LICENSE | sed -e 's/\//\\\//g' | sed -e 's/\+/\\\+/g'| sed -e 's/\=/\\\=/g')
  sed -i -e "s/<property name=\"license\">lkey<\/property>/<property name=\"license\">$CROWD_LICENSE<\/property>/" $CROWD_HOME/crowd.cfg.xml
  chown -R atlassian "$CROWD_HOME"
  umask 0027
  exec gosu atlassian /opt/atlassian/apache-tomcat/bin/catalina.sh run
fi
